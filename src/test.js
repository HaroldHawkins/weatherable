const currentConditions =
{
    "coord": {
        "lon": -74.03,
        "lat": 40.63
    },
    "weather": [
        {
            "id": 612,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 302.66,
        "feels_like": 307.65,
        "temp_min": 301.15,
        "temp_max": 304.26,
        "pressure": 1016,
        "humidity": 74
    },
    "visibility": 10000,
    "wind": {
        "speed": 1.5,
        "deg": 210
    },
    "clouds": {
        "all": 1
    },
    "dt": 1597158003,
    "sys": {
        "type": 1,
        "id": 4610,
        "country": "US",
        "sunrise": 1597140193,
        "sunset": 1597190365
    },
    "timezone": -14400,
    "id": 0,
    "name": "Brooklyn",
    "cod": 200
};

const allConditions = 
{
    "lat": 40.63,
    "lon": -74.03,
    "timezone": "America/New_York",
    "timezone_offset": -14400,
    "current": {
        "dt": 1597146187,
        "sunrise": 1597140193,
        "sunset": 1597190365,
        "temp": 298.44,
        "feels_like": 302.72,
        "pressure": 1016,
        "humidity": 88,
        "dew_point": 296.31,
        "uvi": 9.2,
        "clouds": 1,
        "visibility": 10000,
        "wind_speed": 1.5,
        "wind_deg": 250,
        "weather": [
            {
                "id": 800,
                "main": "Clear",
                "description": "clear sky",
                "icon": "01d"
            }
        ]
    },
    "minutely": [
        {
            "dt": 1597146240,
            "precipitation": 0
        },
        {
            "dt": 1597146300,
            "precipitation": 0
        },
        {
            "dt": 1597146360,
            "precipitation": 0
        },
        {
            "dt": 1597146420,
            "precipitation": 0
        },
        {
            "dt": 1597146480,
            "precipitation": 0
        },
        {
            "dt": 1597146540,
            "precipitation": 0
        },
        {
            "dt": 1597146600,
            "precipitation": 0
        },
        {
            "dt": 1597146660,
            "precipitation": 0
        },
        {
            "dt": 1597146720,
            "precipitation": 0
        },
        {
            "dt": 1597146780,
            "precipitation": 0
        },
        {
            "dt": 1597146840,
            "precipitation": 0
        },
        {
            "dt": 1597146900,
            "precipitation": 0
        },
        {
            "dt": 1597146960,
            "precipitation": 0
        },
        {
            "dt": 1597147020,
            "precipitation": 0
        },
        {
            "dt": 1597147080,
            "precipitation": 0
        },
        {
            "dt": 1597147140,
            "precipitation": 0
        },
        {
            "dt": 1597147200,
            "precipitation": 0
        },
        {
            "dt": 1597147260,
            "precipitation": 0
        },
        {
            "dt": 1597147320,
            "precipitation": 0
        },
        {
            "dt": 1597147380,
            "precipitation": 0
        },
        {
            "dt": 1597147440,
            "precipitation": 0
        },
        {
            "dt": 1597147500,
            "precipitation": 0
        },
        {
            "dt": 1597147560,
            "precipitation": 0
        },
        {
            "dt": 1597147620,
            "precipitation": 0
        },
        {
            "dt": 1597147680,
            "precipitation": 0
        },
        {
            "dt": 1597147740,
            "precipitation": 0
        },
        {
            "dt": 1597147800,
            "precipitation": 0
        },
        {
            "dt": 1597147860,
            "precipitation": 0
        },
        {
            "dt": 1597147920,
            "precipitation": 0
        },
        {
            "dt": 1597147980,
            "precipitation": 0
        },
        {
            "dt": 1597148040,
            "precipitation": 0
        },
        {
            "dt": 1597148100,
            "precipitation": 0
        },
        {
            "dt": 1597148160,
            "precipitation": 0
        },
        {
            "dt": 1597148220,
            "precipitation": 0
        },
        {
            "dt": 1597148280,
            "precipitation": 0
        },
        {
            "dt": 1597148340,
            "precipitation": 0
        },
        {
            "dt": 1597148400,
            "precipitation": 0
        },
        {
            "dt": 1597148460,
            "precipitation": 0
        },
        {
            "dt": 1597148520,
            "precipitation": 0
        },
        {
            "dt": 1597148580,
            "precipitation": 0
        },
        {
            "dt": 1597148640,
            "precipitation": 0
        },
        {
            "dt": 1597148700,
            "precipitation": 0
        },
        {
            "dt": 1597148760,
            "precipitation": 0
        },
        {
            "dt": 1597148820,
            "precipitation": 0
        },
        {
            "dt": 1597148880,
            "precipitation": 0
        },
        {
            "dt": 1597148940,
            "precipitation": 0
        },
        {
            "dt": 1597149000,
            "precipitation": 0
        },
        {
            "dt": 1597149060,
            "precipitation": 0
        },
        {
            "dt": 1597149120,
            "precipitation": 0
        },
        {
            "dt": 1597149180,
            "precipitation": 0
        },
        {
            "dt": 1597149240,
            "precipitation": 0
        },
        {
            "dt": 1597149300,
            "precipitation": 0
        },
        {
            "dt": 1597149360,
            "precipitation": 0
        },
        {
            "dt": 1597149420,
            "precipitation": 0
        },
        {
            "dt": 1597149480,
            "precipitation": 0
        },
        {
            "dt": 1597149540,
            "precipitation": 0
        },
        {
            "dt": 1597149600,
            "precipitation": 0
        },
        {
            "dt": 1597149660,
            "precipitation": 0
        },
        {
            "dt": 1597149720,
            "precipitation": 0
        },
        {
            "dt": 1597149780,
            "precipitation": 0
        },
        {
            "dt": 1597149840,
            "precipitation": 0
        }
    ],
    "hourly": [
        {
            "dt": 1597143600,
            "temp": 298.44,
            "feels_like": 301.69,
            "pressure": 1016,
            "humidity": 88,
            "dew_point": 296.31,
            "clouds": 1,
            "visibility": 10000,
            "wind_speed": 2.98,
            "wind_deg": 202,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.08
        },
        {
            "dt": 1597147200,
            "temp": 298.52,
            "feels_like": 301.2,
            "pressure": 1016,
            "humidity": 81,
            "dew_point": 295.02,
            "clouds": 13,
            "visibility": 10000,
            "wind_speed": 2.79,
            "wind_deg": 206,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.08
        },
        {
            "dt": 1597150800,
            "temp": 299.17,
            "feels_like": 301.55,
            "pressure": 1015,
            "humidity": 74,
            "dew_point": 294.18,
            "clouds": 28,
            "visibility": 10000,
            "wind_speed": 2.59,
            "wind_deg": 199,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1597154400,
            "temp": 300.27,
            "feels_like": 301.93,
            "pressure": 1015,
            "humidity": 65,
            "dew_point": 293.12,
            "clouds": 17,
            "visibility": 10000,
            "wind_speed": 2.88,
            "wind_deg": 193,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.04
        },
        {
            "dt": 1597158000,
            "temp": 301.17,
            "feels_like": 302.15,
            "pressure": 1015,
            "humidity": 60,
            "dew_point": 292.68,
            "clouds": 13,
            "visibility": 10000,
            "wind_speed": 3.55,
            "wind_deg": 175,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.04
        },
        {
            "dt": 1597161600,
            "temp": 301.59,
            "feels_like": 301.83,
            "pressure": 1015,
            "humidity": 58,
            "dew_point": 292.81,
            "clouds": 11,
            "visibility": 10000,
            "wind_speed": 4.51,
            "wind_deg": 167,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.02
        },
        {
            "dt": 1597165200,
            "temp": 301.71,
            "feels_like": 301.11,
            "pressure": 1014,
            "humidity": 59,
            "dew_point": 293.01,
            "clouds": 19,
            "visibility": 10000,
            "wind_speed": 5.97,
            "wind_deg": 160,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.02
        },
        {
            "dt": 1597168800,
            "temp": 301.71,
            "feels_like": 300.32,
            "pressure": 1014,
            "humidity": 59,
            "dew_point": 293.09,
            "clouds": 19,
            "visibility": 10000,
            "wind_speed": 7.1,
            "wind_deg": 156,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.02
        },
        {
            "dt": 1597172400,
            "temp": 301.66,
            "feels_like": 299.45,
            "pressure": 1014,
            "humidity": 60,
            "dew_point": 293.25,
            "clouds": 31,
            "visibility": 10000,
            "wind_speed": 8.42,
            "wind_deg": 157,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0.04
        },
        {
            "dt": 1597176000,
            "temp": 301.47,
            "feels_like": 298.96,
            "pressure": 1013,
            "humidity": 61,
            "dew_point": 293.35,
            "clouds": 64,
            "visibility": 10000,
            "wind_speed": 8.91,
            "wind_deg": 163,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0
        },
        {
            "dt": 1597179600,
            "temp": 301.28,
            "feels_like": 299.07,
            "pressure": 1013,
            "humidity": 63,
            "dew_point": 293.68,
            "clouds": 62,
            "visibility": 10000,
            "wind_speed": 8.72,
            "wind_deg": 165,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0.01
        },
        {
            "dt": 1597183200,
            "temp": 300.96,
            "feels_like": 299.18,
            "pressure": 1013,
            "humidity": 64,
            "dew_point": 293.65,
            "clouds": 52,
            "visibility": 10000,
            "wind_speed": 8.07,
            "wind_deg": 169,
            "weather": [
                {
                    "id": 803,
                    "main": "Clouds",
                    "description": "broken clouds",
                    "icon": "04d"
                }
            ],
            "pop": 0.01
        },
        {
            "dt": 1597186800,
            "temp": 300.58,
            "feels_like": 299.18,
            "pressure": 1013,
            "humidity": 65,
            "dew_point": 293.63,
            "clouds": 44,
            "visibility": 10000,
            "wind_speed": 7.45,
            "wind_deg": 171,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "pop": 0.1
        },
        {
            "dt": 1597190400,
            "temp": 300.31,
            "feels_like": 299.8,
            "pressure": 1013,
            "humidity": 67,
            "dew_point": 293.65,
            "clouds": 37,
            "visibility": 10000,
            "wind_speed": 6.35,
            "wind_deg": 178,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03n"
                }
            ],
            "pop": 0.24
        },
        {
            "dt": 1597194000,
            "temp": 300.26,
            "feels_like": 300.31,
            "pressure": 1014,
            "humidity": 67,
            "dew_point": 293.74,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 5.51,
            "wind_deg": 186,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.16
        },
        {
            "dt": 1597197600,
            "temp": 300.01,
            "feels_like": 300.24,
            "pressure": 1014,
            "humidity": 69,
            "dew_point": 293.94,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 5.43,
            "wind_deg": 191,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.08
        },
        {
            "dt": 1597201200,
            "temp": 299.68,
            "feels_like": 300.27,
            "pressure": 1014,
            "humidity": 72,
            "dew_point": 294.3,
            "clouds": 10,
            "visibility": 10000,
            "wind_speed": 5.18,
            "wind_deg": 201,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.38,
            "rain": {
                "1h": 0.15
            }
        },
        {
            "dt": 1597204800,
            "temp": 299.31,
            "feels_like": 300.19,
            "pressure": 1014,
            "humidity": 74,
            "dew_point": 294.33,
            "clouds": 9,
            "visibility": 10000,
            "wind_speed": 4.83,
            "wind_deg": 202,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.36,
            "rain": {
                "1h": 0.53
            }
        },
        {
            "dt": 1597208400,
            "temp": 298.91,
            "feels_like": 299.91,
            "pressure": 1014,
            "humidity": 75,
            "dew_point": 294.33,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 4.54,
            "wind_deg": 207,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.39,
            "rain": {
                "1h": 0.41
            }
        },
        {
            "dt": 1597212000,
            "temp": 298.64,
            "feels_like": 300.03,
            "pressure": 1014,
            "humidity": 77,
            "dew_point": 294.41,
            "clouds": 6,
            "visibility": 10000,
            "wind_speed": 4.11,
            "wind_deg": 214,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.37
        },
        {
            "dt": 1597215600,
            "temp": 298.41,
            "feels_like": 300.06,
            "pressure": 1014,
            "humidity": 78,
            "dew_point": 294.44,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.73,
            "wind_deg": 216,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01n"
                }
            ],
            "pop": 0.15
        },
        {
            "dt": 1597219200,
            "temp": 298.2,
            "feels_like": 300.12,
            "pressure": 1014,
            "humidity": 79,
            "dew_point": 294.5,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.34,
            "wind_deg": 213,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.23,
            "rain": {
                "1h": 0.31
            }
        },
        {
            "dt": 1597222800,
            "temp": 298.12,
            "feels_like": 300.34,
            "pressure": 1014,
            "humidity": 80,
            "dew_point": 294.54,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 3.01,
            "wind_deg": 216,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.38,
            "rain": {
                "1h": 0.73
            }
        },
        {
            "dt": 1597226400,
            "temp": 298.07,
            "feels_like": 300.5,
            "pressure": 1015,
            "humidity": 80,
            "dew_point": 294.55,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.67,
            "wind_deg": 220,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.32,
            "rain": {
                "1h": 0.51
            }
        },
        {
            "dt": 1597230000,
            "temp": 298.07,
            "feels_like": 300.77,
            "pressure": 1015,
            "humidity": 80,
            "dew_point": 294.58,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.29,
            "wind_deg": 222,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.27,
            "rain": {
                "1h": 0.25
            }
        },
        {
            "dt": 1597233600,
            "temp": 298.84,
            "feels_like": 301.65,
            "pressure": 1016,
            "humidity": 77,
            "dew_point": 294.53,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 2.21,
            "wind_deg": 232,
            "weather": [
                {
                    "id": 800,
                    "main": "Clear",
                    "description": "clear sky",
                    "icon": "01d"
                }
            ],
            "pop": 0.27
        },
        {
            "dt": 1597237200,
            "temp": 299.87,
            "feels_like": 302.7,
            "pressure": 1016,
            "humidity": 71,
            "dew_point": 294.39,
            "clouds": 0,
            "visibility": 10000,
            "wind_speed": 1.94,
            "wind_deg": 245,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.38,
            "rain": {
                "1h": 0.12
            }
        },
        {
            "dt": 1597240800,
            "temp": 300.93,
            "feels_like": 304.07,
            "pressure": 1016,
            "humidity": 66,
            "dew_point": 294.16,
            "clouds": 7,
            "visibility": 10000,
            "wind_speed": 1.37,
            "wind_deg": 235,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.34,
            "rain": {
                "1h": 0.1
            }
        },
        {
            "dt": 1597244400,
            "temp": 302,
            "feels_like": 305.1,
            "pressure": 1016,
            "humidity": 61,
            "dew_point": 293.88,
            "clouds": 18,
            "visibility": 10000,
            "wind_speed": 1.24,
            "wind_deg": 213,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.49
        },
        {
            "dt": 1597248000,
            "temp": 302.84,
            "feels_like": 305.53,
            "pressure": 1015,
            "humidity": 58,
            "dew_point": 293.73,
            "clouds": 22,
            "visibility": 10000,
            "wind_speed": 1.8,
            "wind_deg": 178,
            "weather": [
                {
                    "id": 801,
                    "main": "Clouds",
                    "description": "few clouds",
                    "icon": "02d"
                }
            ],
            "pop": 0.53
        },
        {
            "dt": 1597251600,
            "temp": 303.13,
            "feels_like": 304.77,
            "pressure": 1016,
            "humidity": 56,
            "dew_point": 293.53,
            "clouds": 33,
            "visibility": 10000,
            "wind_speed": 3.09,
            "wind_deg": 168,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.62,
            "rain": {
                "1h": 0.13
            }
        },
        {
            "dt": 1597255200,
            "temp": 302.91,
            "feels_like": 304.03,
            "pressure": 1016,
            "humidity": 57,
            "dew_point": 293.55,
            "clouds": 44,
            "visibility": 10000,
            "wind_speed": 3.89,
            "wind_deg": 167,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.68,
            "rain": {
                "1h": 0.23
            }
        },
        {
            "dt": 1597258800,
            "temp": 302.51,
            "feels_like": 303.16,
            "pressure": 1016,
            "humidity": 58,
            "dew_point": 293.52,
            "clouds": 99,
            "visibility": 10000,
            "wind_speed": 4.5,
            "wind_deg": 166,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.62,
            "rain": {
                "1h": 0.27
            }
        },
        {
            "dt": 1597262400,
            "temp": 302.52,
            "feels_like": 302.81,
            "pressure": 1015,
            "humidity": 57,
            "dew_point": 293.32,
            "clouds": 82,
            "visibility": 10000,
            "wind_speed": 4.83,
            "wind_deg": 161,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.62,
            "rain": {
                "1h": 0.9
            }
        },
        {
            "dt": 1597266000,
            "temp": 302.14,
            "feels_like": 301.93,
            "pressure": 1015,
            "humidity": 59,
            "dew_point": 293.4,
            "clouds": 76,
            "visibility": 10000,
            "wind_speed": 5.69,
            "wind_deg": 164,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.72,
            "rain": {
                "1h": 1.06
            }
        },
        {
            "dt": 1597269600,
            "temp": 301.3,
            "feels_like": 301.11,
            "pressure": 1015,
            "humidity": 64,
            "dew_point": 293.88,
            "clouds": 72,
            "visibility": 10000,
            "wind_speed": 6.03,
            "wind_deg": 187,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.69,
            "rain": {
                "1h": 1.96
            }
        },
        {
            "dt": 1597273200,
            "temp": 300.02,
            "feels_like": 300.95,
            "pressure": 1016,
            "humidity": 73,
            "dew_point": 294.89,
            "clouds": 76,
            "visibility": 10000,
            "wind_speed": 5.09,
            "wind_deg": 179,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "pop": 0.74,
            "rain": {
                "1h": 2.06
            }
        },
        {
            "dt": 1597276800,
            "temp": 299.87,
            "feels_like": 301.1,
            "pressure": 1016,
            "humidity": 72,
            "dew_point": 294.53,
            "clouds": 80,
            "visibility": 10000,
            "wind_speed": 4.39,
            "wind_deg": 159,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.74,
            "rain": {
                "1h": 0.6
            }
        },
        {
            "dt": 1597280400,
            "temp": 299.63,
            "feels_like": 300.52,
            "pressure": 1017,
            "humidity": 72,
            "dew_point": 294.25,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 4.71,
            "wind_deg": 173,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.68,
            "rain": {
                "1h": 0.27
            }
        },
        {
            "dt": 1597284000,
            "temp": 299.45,
            "feels_like": 300.51,
            "pressure": 1017,
            "humidity": 72,
            "dew_point": 294.21,
            "clouds": 93,
            "visibility": 10000,
            "wind_speed": 4.35,
            "wind_deg": 183,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.69,
            "rain": {
                "1h": 0.51
            }
        },
        {
            "dt": 1597287600,
            "temp": 299.25,
            "feels_like": 301.56,
            "pressure": 1017,
            "humidity": 75,
            "dew_point": 294.61,
            "clouds": 85,
            "visibility": 10000,
            "wind_speed": 2.91,
            "wind_deg": 199,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.8,
            "rain": {
                "1h": 1.44
            }
        },
        {
            "dt": 1597291200,
            "temp": 299.1,
            "feels_like": 302,
            "pressure": 1018,
            "humidity": 77,
            "dew_point": 294.84,
            "clouds": 88,
            "visibility": 10000,
            "wind_speed": 2.27,
            "wind_deg": 212,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.8,
            "rain": {
                "1h": 1.02
            }
        },
        {
            "dt": 1597294800,
            "temp": 298.88,
            "feels_like": 302.03,
            "pressure": 1018,
            "humidity": 78,
            "dew_point": 294.89,
            "clouds": 91,
            "visibility": 10000,
            "wind_speed": 1.92,
            "wind_deg": 206,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.8,
            "rain": {
                "1h": 0.58
            }
        },
        {
            "dt": 1597298400,
            "temp": 298.57,
            "feels_like": 302.15,
            "pressure": 1018,
            "humidity": 80,
            "dew_point": 294.92,
            "clouds": 93,
            "visibility": 10000,
            "wind_speed": 1.38,
            "wind_deg": 204,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.8,
            "rain": {
                "1h": 1.18
            }
        },
        {
            "dt": 1597302000,
            "temp": 298.43,
            "feels_like": 302.32,
            "pressure": 1017,
            "humidity": 80,
            "dew_point": 294.77,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 0.84,
            "wind_deg": 201,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.77,
            "rain": {
                "1h": 0.28
            }
        },
        {
            "dt": 1597305600,
            "temp": 298.34,
            "feels_like": 301.98,
            "pressure": 1018,
            "humidity": 79,
            "dew_point": 294.51,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 0.98,
            "wind_deg": 7,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.75,
            "rain": {
                "1h": 0.56
            }
        },
        {
            "dt": 1597309200,
            "temp": 298.17,
            "feels_like": 300.97,
            "pressure": 1018,
            "humidity": 78,
            "dew_point": 294.1,
            "clouds": 100,
            "visibility": 10000,
            "wind_speed": 1.91,
            "wind_deg": 55,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.68,
            "rain": {
                "1h": 0.59
            }
        },
        {
            "dt": 1597312800,
            "temp": 297.83,
            "feels_like": 300.21,
            "pressure": 1018,
            "humidity": 77,
            "dew_point": 293.55,
            "clouds": 100,
            "visibility": 8756,
            "wind_speed": 2.14,
            "wind_deg": 65,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10n"
                }
            ],
            "pop": 0.7,
            "rain": {
                "1h": 1.64
            }
        }
    ],
    "daily": [
        {
            "dt": 1597165200,
            "sunrise": 1597140193,
            "sunset": 1597190365,
            "temp": {
                "day": 301.5,
                "min": 298.52,
                "max": 301.5,
                "night": 298.64,
                "eve": 300.31,
                "morn": 298.52
            },
            "feels_like": {
                "day": 300.14,
                "night": 300.03,
                "eve": 299.8,
                "morn": 301.2
            },
            "pressure": 1014,
            "humidity": 60,
            "dew_point": 292.99,
            "wind_speed": 7.1,
            "wind_deg": 156,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 18,
            "pop": 0.37,
            "rain": 1.01,
            "uvi": 9.2
        },
        {
            "dt": 1597251600,
            "sunrise": 1597226652,
            "sunset": 1597276686,
            "temp": {
                "day": 302.91,
                "min": 298.57,
                "max": 302.91,
                "night": 298.57,
                "eve": 299.87,
                "morn": 298.84
            },
            "feels_like": {
                "day": 304.03,
                "night": 302.15,
                "eve": 301.1,
                "morn": 301.65
            },
            "pressure": 1016,
            "humidity": 57,
            "dew_point": 293.55,
            "wind_speed": 3.89,
            "wind_deg": 167,
            "weather": [
                {
                    "id": 502,
                    "main": "Rain",
                    "description": "heavy intensity rain",
                    "icon": "10d"
                }
            ],
            "clouds": 44,
            "pop": 0.8,
            "rain": 13.13,
            "uvi": 8.52
        },
        {
            "dt": 1597338000,
            "sunrise": 1597313112,
            "sunset": 1597363007,
            "temp": {
                "day": 296.96,
                "min": 295.67,
                "max": 297.52,
                "night": 295.67,
                "eve": 296.23,
                "morn": 297.49
            },
            "feels_like": {
                "day": 299.51,
                "night": 297.67,
                "eve": 297.74,
                "morn": 299.63
            },
            "pressure": 1019,
            "humidity": 85,
            "dew_point": 294.31,
            "wind_speed": 2.43,
            "wind_deg": 83,
            "weather": [
                {
                    "id": 502,
                    "main": "Rain",
                    "description": "heavy intensity rain",
                    "icon": "10d"
                }
            ],
            "clouds": 100,
            "pop": 1,
            "rain": 21.99,
            "uvi": 8.36
        },
        {
            "dt": 1597424400,
            "sunrise": 1597399571,
            "sunset": 1597449326,
            "temp": {
                "day": 294.85,
                "min": 294.77,
                "max": 295.55,
                "night": 294.77,
                "eve": 295.55,
                "morn": 295.3
            },
            "feels_like": {
                "day": 293.89,
                "night": 293.34,
                "eve": 295.04,
                "morn": 295.5
            },
            "pressure": 1018,
            "humidity": 85,
            "dew_point": 292.38,
            "wind_speed": 6.03,
            "wind_deg": 92,
            "weather": [
                {
                    "id": 501,
                    "main": "Rain",
                    "description": "moderate rain",
                    "icon": "10d"
                }
            ],
            "clouds": 100,
            "pop": 1,
            "rain": 6.41,
            "uvi": 8.72
        },
        {
            "dt": 1597510800,
            "sunrise": 1597486031,
            "sunset": 1597535644,
            "temp": {
                "day": 298.02,
                "min": 293.73,
                "max": 298.07,
                "night": 293.73,
                "eve": 296.37,
                "morn": 294.77
            },
            "feels_like": {
                "day": 293.58,
                "night": 290.2,
                "eve": 291.61,
                "morn": 292.21
            },
            "pressure": 1018,
            "humidity": 54,
            "dew_point": 288.09,
            "wind_speed": 8.6,
            "wind_deg": 91,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 100,
            "pop": 0.16,
            "uvi": 8.87
        },
        {
            "dt": 1597597200,
            "sunrise": 1597572490,
            "sunset": 1597621961,
            "temp": {
                "day": 296.93,
                "min": 293.78,
                "max": 297.1,
                "night": 294.28,
                "eve": 295.89,
                "morn": 293.78
            },
            "feels_like": {
                "day": 292.98,
                "night": 291.46,
                "eve": 292.61,
                "morn": 290.12
            },
            "pressure": 1019,
            "humidity": 59,
            "dew_point": 288.55,
            "wind_speed": 8.1,
            "wind_deg": 88,
            "weather": [
                {
                    "id": 804,
                    "main": "Clouds",
                    "description": "overcast clouds",
                    "icon": "04d"
                }
            ],
            "clouds": 99,
            "pop": 0.02,
            "uvi": 8.1
        },
        {
            "dt": 1597683600,
            "sunrise": 1597658950,
            "sunset": 1597708277,
            "temp": {
                "day": 297.58,
                "min": 293.79,
                "max": 297.58,
                "night": 295.36,
                "eve": 296.35,
                "morn": 293.79
            },
            "feels_like": {
                "day": 296.16,
                "night": 295.85,
                "eve": 295.23,
                "morn": 291.34
            },
            "pressure": 1013,
            "humidity": 67,
            "dew_point": 291.21,
            "wind_speed": 5.95,
            "wind_deg": 85,
            "weather": [
                {
                    "id": 500,
                    "main": "Rain",
                    "description": "light rain",
                    "icon": "10d"
                }
            ],
            "clouds": 72,
            "pop": 0.72,
            "rain": 0.72,
            "uvi": 7.7
        },
        {
            "dt": 1597770000,
            "sunrise": 1597745409,
            "sunset": 1597794592,
            "temp": {
                "day": 297.48,
                "min": 295.29,
                "max": 299.05,
                "night": 297.95,
                "eve": 297.95,
                "morn": 295.29
            },
            "feels_like": {
                "day": 298.61,
                "night": 299.36,
                "eve": 299.36,
                "morn": 295.89
            },
            "pressure": 1011,
            "humidity": 65,
            "dew_point": 290.68,
            "wind_speed": 1.97,
            "wind_deg": 170,
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "clouds": 40,
            "pop": 0.03,
            "uvi": 8.22
        }
    ]
}
export { allConditions, currentConditions};