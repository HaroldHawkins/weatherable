import wind1 from './backgrounds/wind1.jpg';
import dayclear1 from './backgrounds/dayclear1.jpg';
import dayclear2 from './backgrounds/dayclear2.jpg';
import dayclear3 from './backgrounds/dayclear3.jpg';
import dayclear4 from './backgrounds/dayclear4.jpg';
import daydrizzle from './backgrounds/daydrizzle.jpg';
import dayovercast1 from './backgrounds/dayovercast1.jpg';
import daypartlycloudy from './backgrounds/daypartlycloudy1.jpg';
import dayrain1 from './backgrounds/dayrain1.jpg';
import dust1 from './backgrounds/dust1.jpg';
import fog1 from './backgrounds/fog1.jpg';
import hail1 from './backgrounds/hail1.jpg';
import haze1 from './backgrounds/haze1.jpg';
import highwind1 from './backgrounds/highwind1.jpg';
import hurricane1 from './backgrounds/hurricane1.jpg';
import lightening1 from './backgrounds/lightening1.jpg';
import lightening2 from './backgrounds/lightening2.jpg';
import nightclear from './backgrounds/nightclear.jpg';
import nightdrizzle1 from './backgrounds/nightdrizzle1.jpg';
import nightrain1 from './backgrounds/nightrain1.jpg';
import rainmix from './backgrounds/rainmix.jpg';
import rainmix2 from './backgrounds/rainmix2.jpg';
import smoke1 from './backgrounds/smoke1.jpg';
import snow1 from './backgrounds/snow1.jpg';
import supercell1 from './backgrounds/supercell1.jpg';
import thumderstorm1 from './backgrounds/thunderstorm1.jpg';
// import wind1 from './backgounds/wind1.jpg';

const backgrounds = [
    [
        thumderstorm1
    ]
];
backgrounds[200] = [
    lightening1,
    lightening2
];
backgrounds[201] = [
    lightening1,
    lightening2
];
backgrounds[202] = [
    lightening1,
    lightening2
];
backgrounds[210] = [
    lightening1,
    lightening2
];
backgrounds[211] = [
    lightening1,
    lightening2
];
backgrounds[212] = [
    lightening1,
    lightening2
];
backgrounds[221] = [
    lightening1,
    lightening2
];
backgrounds[230] = [
    lightening1,
    lightening2
];
backgrounds[231] = [
    lightening1,
    lightening2
];
backgrounds[232] = [
    lightening1,
    lightening2
];
backgrounds[300] = [
    daydrizzle
];
backgrounds[301] = [
    daydrizzle
];
backgrounds[302] = [
    dayrain1
];
backgrounds[310] = [
    rainmix,
    rainmix2
];
backgrounds[311] = [
    dayrain1
];
backgrounds[312] = [
    dayrain1
];
backgrounds[313] = [
    dayrain1
];
backgrounds[314] = [
    dayrain1
];
backgrounds[321] = [
    daydrizzle
];
backgrounds[500] = [
    daydrizzle
];
backgrounds[501] = [
    dayrain1
];
backgrounds[502] = [
    dayrain1
];
backgrounds[503] = [
    dayrain1
];
backgrounds[504] = [
    dayrain1
];
backgrounds[511] = [
    dayrain1
];
backgrounds[520] = [
    dayrain1
];
backgrounds[521] = [
    dayrain1
];
backgrounds[522] = [
    dayrain1
];
backgrounds[531] = [
    dayrain1
];
backgrounds[600] = [
    snow1
];
backgrounds[601] = [
    snow1
];
backgrounds[602] = [
    dayrain1
];
backgrounds[611] = [
    rainmix
];
backgrounds[612] = [
    rainmix
];
backgrounds[615] = [
    rainmix
];
backgrounds[616] = [
    rainmix
];
backgrounds[620] = [
    rainmix
];
backgrounds[621] = [
    snow1
];
backgrounds[622] = [
    snow1
];
backgrounds[701] = [
    dayrain1
];
backgrounds[711] = [
    smoke1
];
backgrounds[721] = [
    haze1
];
backgrounds[731] = [
    dust1
];
backgrounds[741] = [
    fog1
];
backgrounds[761] = [
    dust1
];
backgrounds[762] = [
    dust1
];
backgrounds[771] = [
    wind1
];
backgrounds[781] = [
    supercell1
];
backgrounds[800] = [
    dayclear1,
    dayclear2,
    dayclear3,
    dayclear4
];
backgrounds[801] = [
    wind1
];
backgrounds[802] = [
    wind1
];
backgrounds[803] = [
    wind1
];
backgrounds[804] = [
    dayovercast1
];
backgrounds[900] = [
    supercell1
];
backgrounds[901] = [
    thumderstorm1
];
backgrounds[902] = [
    hurricane1
];
backgrounds[903] = [
    snow1
];
backgrounds[904] = [
    dayclear1,
    dayclear2,
    dayclear3,
    dayclear4
];
backgrounds[905] = [
    wind1
];
backgrounds[906] = [
    hail1
];
backgrounds[957] = [
    highwind1
];
backgrounds[10000] = [
    nightclear
];

export default backgrounds;