import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../index.css';
import fiOWM from '../futureicons';
import axios from 'axios';

class Daily extends Component{
    constructor(props){
        super();
        this.state = {props};
    }

    componentWillMount(){
        let lat = 40.63;
        let lon =-74.03;
        if ("geolocation" in navigator) {
          navigator.geolocation.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
          });        
        } else {
          console.log("Not Available");
        }
            axios.get(
            "https://api.openweathermap.org/data/2.5/onecall",{
            params:
              {
                appid:"dfba23226395d24a4c6293b1c3e8821b",
                lat: lat,
                lon: lon
              }
            }
    
          )
          .then((response1)=>{
            this.setState({props:{
                    forcast:response1.data.daily
                },
                units: this.props.units
            });
          });
        }

    componentDidMount(){
    }

    Fahrenheit = (t) => {
        return Math.round(((t-273.15)*1.8)+32);
    }

    Celsius = (t) => {
        return Math.round(t-273.15);
    }
    
    ConvertTemp = (t) => {
        return this.state.units=='F'?this.Fahrenheit(t) + '°F':this.Celsius(t) + '°C';
    }

    getDateString = (t) => {
        let dt = new Date(t*1000);
        let d = dt.getDay();
        let ddayname = 'Sun';
        switch(d){
            case 0:
                ddayname = 'Sun';
                break;
            case 1:
                ddayname = 'Mon';
                break;
            case 2:
                ddayname = 'Tue';
                break;
            case 3:
                ddayname = 'Wed';
                break;
            case 4:
                ddayname = 'Thu';
                break;
            case 5:
                ddayname = 'Fri';
                break;
            case 6:
                ddayname = 'Sat';
                break;
        }
        d = dt.getDate()
        return ddayname + ' ' + d;
    }

    getDirectionSymbol = (deg) => {
        let sym = "\u2B06"; // N default
        if((deg >= 338) || (deg < 22))
            sym = "\u2B06"; //N
        if((deg >= 22) && (deg < 68))
            sym = "\u2197";// NE
        if((deg >= 68) && (deg < 113))
            sym = "\u27A1";// E
        if((deg >= 113) && (deg < 158))
            sym = "\u2198"; // SE
        if((deg >= 158) && (deg < 202))
            sym = "\u2B07"; // S
        if((deg >= 202) && (deg < 247))
            sym = "\u2199"; // SW
        if((deg >= 247) && (deg < 292))
            sym = "\u2B05"; // W
        if((deg >= 292) && (deg < 338))
            sym ="\u2196"; // NW
        return sym;
    }

    render(){
        let section = "";

        let rows = [];
        let dt = 0;
        let dti = 0;
        for(let i = 0; i < 7; i++){
            dt = this.state.props.forcast[i].dt;
            dti = i+1;
            rows[i] = 
                <tr>
                    <td className="outlineFX dailydate">{this.getDateString(this.state.props.forcast[dti].dt)}</td>
                    <td className='outlineFX dailyimgtd' style={{textAlign:"center"}}><img title={this.state.props.forcast[dti].weather[0].main} src={fiOWM[this.state.props.forcast[dti].weather[0].id]} className="dailyimg"/></td>
                    <td>
                        <table>
                            <tbody>
                                <tr>
                                    <td className="dailytemps outlineFX">{this.ConvertTemp(this.state.props.forcast[dti].temp.max)} / {this.ConvertTemp(this.state.props.forcast[dti].temp.min)}</td>
                                </tr>
                                <tr className="dailywindtr">
                                    <td className="dailywindsp outlineFX">{this.getDirectionSymbol(this.state.props.forcast[dti].wind_deg)} Wind Speed {this.state.props.forcast[dti].wind_speed}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>;
        }
        return(
            <table style={{textAlign:'center'}} className="dailytable">
                <tbody>
                    {rows}
                </tbody>
            </table>
        );
    }
}

export default Daily;