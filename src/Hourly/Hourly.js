import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../index.css';
import fiOWM from '../futureicons';
import axios from 'axios';

class Hourly extends Component{
    constructor(props){
        super();
        this.state = {props};
    }

    componentWillMount(){
        let lat = 40.63;
        let lon =-74.03;
        if ("geolocation" in navigator) {
          navigator.geolocation.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
          });        
        } else {
          console.log("Not Available");
        }
            axios.get(
            "https://api.openweathermap.org/data/2.5/onecall",{
            params:
              {
                appid:"dfba23226395d24a4c6293b1c3e8821b",
                lat: lat,
                lon: lon
              }
            }
    
          )
          .then((response1)=>{
            this.setState({props:{
                    forcast:response1.data.hourly
                },
                units: this.props.units
            });
          });
        }

    componentDidMount(){
    }

    Fahrenheit = (t) => {
        return Math.round(((t-273.15)*1.8)+32);
    }

    Celsius = (t) => {
        return Math.round(t-273.15);
    }
    
    ConvertTemp = (t) => {
        return this.state.units=='F'?this.Fahrenheit(t) + '°F':this.Celsius(t) + '°C';
    }

    getHour(t){
        let dt = new Date(t*1000);
        let n = dt.getHours();
        if(n >= 12){
            n == 12 ? n = 12:n = n - 12;
            n = n + "pm";
        }
        else{
            n == 0 ? n = 12 : n = n;
            n = n + "am";
        }
        return n;
    }

    render(){
        let section = "";
        const sliced = this.state.props.forcast.slice(0,this.state.props.hours); // 6 or 12 probably

        let row1 = [];
        let row2 = [];
            let dt = 0;
            for(let i = 0; i < 6; i++){
                dt = this.state.props.forcast[i].dt;
                row1[i] = <td className='outlineFX hourlytd' style={{textAlign:"center"}}><span className="outlineFX">{this.getHour(dt)}</span><img title={this.state.props.forcast[i].weather[0].main} src={fiOWM[this.state.props.forcast[i].weather[0].id]} className="hourlyimg"/></td>;
                row2[i] = <td className='outlineFX hourlytd' style={{verticalAlign:"top"}}>{ this.ConvertTemp(this.state.props.forcast[i].temp)}<br /><div className="outlineFX hourlyweatherdescription">{this.state.props.forcast[i].weather[0].description}</div></td>
            }
        return(
            <table style={{textAlign:'center'}} className="hourlytable">
                <tbody>
                    <tr>{row1}</tr>
                    <tr>{row2}</tr>
                </tbody>
            </table>
        );
    }
}

export default Hourly;