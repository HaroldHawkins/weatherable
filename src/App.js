import React, {Component} from 'react';
import CurrentConditions from './CurrentConditions/CurrentConditions';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {allConditions,currentConditions} from './test';
import axios from 'axios';
import Hourly from './Hourly/Hourly';
import backgrounds from './backgrounds';
import Daily from './Daily/Daily';

class App extends Component{
  constructor(){
    super();
    this.state = {
      appBackground:"url(./backgrounds/dayclear1.jpg)",
      currentConditions: currentConditions,
      allConditions: allConditions
    }
  }

  getData = () =>{
    let lat = 40.63;
    let lon =-74.03;
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition(function(position) {
        lat = position.coords.latitude;
        lon = position.coords.longitude;
      });        
    } else {
      console.log("Not Available");
    }
      axios.get(
        "https://api.openweathermap.org/data/2.5/onecall",{
        params:
          {
            appid:"dfba23226395d24a4c6293b1c3e8821b",
            lat: lat,
            lon: lon
          }
        }

      )
      .then((response1)=>{
        this.setState({allConditions:response1.data});
        axios.get(
          "https://api.openweathermap.org/data/2.5/weather",{
              params:{
              appid:"dfba23226395d24a4c6293b1c3e8821b",
              lat: lat,
              lon: lon
            }
          }
        )
        .then((response2)=>{
          this.setState({currentConditions:response2.data});
          this.forceUpdate();
        });
      });

  }

  componentWillMount = () =>{
    this.getData();
  }

  componentDidMount = () =>{

  }

  render() {
    let bi = backgrounds[this.state.currentConditions.weather[0].id][Math.floor(Math.random()*backgrounds[this.state.currentConditions.weather[0].id].length)];
    if((this.state.currentConditions.dt > this.state.currentConditions.sys.sunset) || (this.state.currentConditions.dt < this.state.currentConditions.sys.sunrise))
      if(this.state.currentConditions.weather[0].id === 800)
        bi = backgrounds[10000][0];
    let bgimage = "url(" + bi + ")";
    let appStyle = {backgroundImage: bgimage, overflow:"auto"}
    return (
      <div className="App container h-100">
        <div className="row h-100 CurrentMainDiv" style={appStyle}>
          <div className="col-md-12 viewdiv">
              <CurrentConditions units="F" current={this.state.allConditions.current} location={this.state.currentConditions.name}/>
              <div className="hourlyouterdiv" style={{display:"inline-block",marginLeft:"16px",marginRight:"16px", textAlign:"center"}}><Hourly units="F" timezone={this.state.currentConditions.timezone} forcast={this.state.allConditions.hourly} hours="6"/></div><br />
              <div className="Dailyouterdiv" style={{display:"inline-block",marginLeft:"16px",marginRight:"16px", textAlign:"center"}}><Daily units="F" timezone={this.state.currentConditions.timezone} forcast={this.state.allConditions.daily} days="7"/></div>
          </div>
        </div>
      </div>
    );
  }
}
export default App;
