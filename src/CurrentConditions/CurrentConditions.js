import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../index.css';
import wiOWM from '../icons';


class CurrentConditions extends Component{
    constructor(props){
        super();
        this.state = {props};
    }
    
    componentWillMount(){
        this.setState({units:"F"});//
    }

    componentDidMount(){
    }

    getData(){
        const axios = require('axios');
        const data = 
        {
            "units": "F",
            "coord": {
                "lon": -74.03,
                "lat": 40.63
            },
            "weather": [
                {
                    "id": 802,
                    "main": "Clouds",
                    "description": "scattered clouds",
                    "icon": "03d"
                }
            ],
            "base": "stations",
            "main": {
                "temp": 298.09,
                "feels_like": 302.03,
                "temp_min": 297.04,
                "temp_max": 299.26,
                "pressure": 1018,
                "humidity": 94
            },
            "visibility": 10000,
            "wind": {
                "speed": 2.6,
                "deg": 250
            },
            "clouds": {
                "all": 40
            },
            "dt": 1597062054,
            "sys": {
                "type": 1,
                "id": 4610,
                "country": "US",
                "sunrise": 1597053733,
                "sunset": 1597104042
            },
            "timezone": -14400,
            "id": 0,
            "name": "Brooklyn",
            "cod": 200
        }
            this.setState(data);
            if(this.state.units === "F"){
                if(this.state.main){
                    const t = this.Fahrenheit(this.state.main.temp);
                    const fl = this.Fahrenheit(this.state.main.feels_like);
                    this.setState({main:{temp: t, feels_like: fl}});
                }
            }
    }

    Fahrenheit = (t) => {
        return Math.round(((t-273.15)*1.8)+32);
    }

    Celsius = (t) => {
        return Math.round(t-273.15);
    }

    render(){
        let cond1 = "";
        const timenow = Math.floor(Date.now() / 1000);
        if((timenow > this.props.current.sunset) || (timenow < this.props.current.sunrise) && this.props.current.weather[0].id === 800){
            cond1 =
                <img className="CurrentMainDiv conditionsvg" src={wiOWM[10000]} style={{maxWidth:"200px",textAlign:"center"}} title={this.props.current.weather[0].main} />
            ;
        }
        else if((timenow < this.props.current.weather[0].sunset) && (timenow > this.props.current.weather[0].sunrise) && this.props.current.weather[0].id === 800){
            cond1 =
                <img className="CurrentMainDiv conditionsvg" src={wiOWM[10000]} style={{maxWidth:"200px",textAlign:"center"}} title={this.props.current.weather[0].main} />
            ;
        }
        else{
            cond1 =
                <img className="CurrentMainDiv conditionsvg" src={wiOWM[this.props.current.weather[0].id]} style={{maxWidth:"200px",textAlign:"center"}} title={this.props.current.weather[0].main}/>
            ;
        }
        const temp = this.props.current.temp;
        let u = "F";
        if(this.state.units == "F")
            u="F";
        else
            u="C";
        return(
                <div>
                <div className="CurrentMainDiv" style={{display:"inline-block",marginLeft:"16px",marginRight:"16px", textAlign:"center"}}>
                    <h3 className="outlineFX">{this.props.location}</h3>
                    <span className="outlineFX">{this.props.current.weather[0].description}</span><br />
                    {cond1}<br /><span style={{fontSize: "1.5em", fontWeight: "bold"}} className="outlineFX">{this.state.units == "F"? this.Fahrenheit(temp):this.Celsius(temp)}&deg;</span><span style={{fontSize: "1.5em", fontWeight: "bold"}} className="outlineFX">{u}</span><br />
                    <span className="outlineFX">Feels like {this.Fahrenheit(this.props.current.feels_like)}&deg;</span>
                </div>
                </div>
        )
    }
}

export default CurrentConditions;